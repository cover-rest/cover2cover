import sys
import re
import os.path

from lxml import etree
from io import BytesIO


# branch-rate="0.0" complexity="0.0" line-rate="1.0"
# branch="true" hits="1" number="86"

# TODO convert to class


def find_lines(j_package, _filename):
    """Return all <line> elements for a given source file in a package."""
    lines = list()
    source_files = j_package.findall("sourcefile")
    for sourcefile in source_files:
        if sourcefile.attrib.get("name") == os.path.basename(_filename):
            lines = lines + sourcefile.findall("line")
    return lines


def line_is_after(jm, start_line):
    return int(jm.attrib.get('line', 0)) > start_line


def method_lines(j_method, j_methods, j_lines):
    """Filter the lines from the given set of j_lines that apply to the given j_method."""
    start_line = int(j_method.attrib.get('line', 0))
    larger = list(int(jm.attrib.get('line', 0)) for jm in j_methods if line_is_after(jm, start_line))
    end_line = min(larger) if len(larger) else 99999999

    for j_line in j_lines:
        if start_line <= int(j_line.attrib['nr']) < end_line:
            yield j_line


def convert_lines(j_lines, into):
    """Convert the JaCoCo <line> elements into Cobertura <line> elements, add them under the given element."""
    c_lines = etree.SubElement(into, 'lines')
    for j_line in j_lines:
        mb = int(j_line.attrib['mb'])
        cb = int(j_line.attrib['cb'])
        ci = int(j_line.attrib['ci'])

        cline = etree.SubElement(c_lines, 'line')
        cline.set('number', j_line.attrib['nr'])
        cline.set('hits', '1' if ci > 0 else '0')  # Probably not true but no way to know from JaCoCo XML file

        if mb + cb > 0:
            percentage = str(int(100 * (float(cb) / (float(cb) + float(mb))))) + '%'
            cline.set('branch', 'true')
            cline.set('condition-coverage', percentage + ' (' + str(cb) + '/' + str(cb + mb) + ')')

            cond = etree.SubElement(etree.SubElement(cline, 'conditions'), 'condition')
            cond.set('number', '0')
            cond.set('type', 'jump')
            cond.set('coverage', percentage)
        else:
            cline.set('branch', 'false')


def guess_filename(path_to_class):
    m = re.match('([^$]*)', path_to_class)
    return (m.group(1) if m else path_to_class) + '.java'


def add_counters(source, target):
    line_counter, line_count = counter(source, 'LINE')
    branch_counter, branch_count = counter(source, 'BRANCH')
    target.set('line-rate', line_counter)
    target.set('branch-rate', branch_counter)
    target.set('complexity', counter(source, 'COMPLEXITY', sum_hits)[0])
    return line_count, branch_count


def fraction(covered, missed):
    return covered / (covered + missed)


def sum_hits(covered, missed):
    return covered + missed


def counter(source, _type, operation=fraction):
    cs = source.findall('counter')
    c = next((ct for ct in cs if ct.attrib.get('type') == _type), None)

    if c is not None:
        covered = float(c.attrib['covered'])
        missed = float(c.attrib['missed'])

        return str(operation(covered, missed)), int(covered)
    else:
        return '0.0', 0


def convert_method(j_method, j_lines):
    c_method = etree.Element('method')
    c_method.set('name', j_method.attrib['name'])
    c_method.set('signature', j_method.attrib['desc'])

    line_count, branch_count = add_counters(j_method, c_method)
    convert_lines(j_lines, c_method)

    return c_method, line_count, branch_count


def convert_class(j_class, j_package):
    line_count, branch_count = (0, 0)
    c_class = etree.Element('class')
    c_class.set('name', j_class.attrib['name'].replace('/', '.'))
    c_class.set('filename', guess_filename(j_class.attrib['name']))

    all_j_lines = list(find_lines(j_package, c_class.attrib['filename']))

    c_methods = etree.SubElement(c_class, 'methods')
    all_j_methods = list(j_class.findall('method'))
    for j_method in all_j_methods:
        j_method_lines = method_lines(j_method, all_j_methods, all_j_lines)
        c_method, _line_count, _branch_count = convert_method(j_method, j_method_lines)
        line_count += _line_count
        branch_count += _branch_count
        c_methods.append(c_method)

    _line_count, _branch_count = add_counters(j_class, c_class)
    line_count += _line_count
    branch_count += _branch_count
    convert_lines(all_j_lines, c_class)

    return c_class, line_count, branch_count


def convert_package(j_package):
    line_count, branch_count = (0, 0)
    c_package = etree.Element('package')
    c_package.attrib['name'] = j_package.attrib['name'].replace('/', '.')

    c_classes = etree.SubElement(c_package, 'classes')
    for j_class in j_package.findall('class'):
        c_class, _line_count, _branch_count = convert_class(j_class, j_package)
        line_count += _line_count
        branch_count += _branch_count
        c_classes.append(c_class)

    _line_count, _branch_count = add_counters(j_package, c_package)
    line_count += _line_count
    branch_count += _branch_count

    return c_package, line_count, branch_count


def convert_root(source, target, _source_roots):
    line_count, branch_count = (0, 0)
    target.set('timestamp', str(int(source.find('sessioninfo').attrib['start']) / 1000))
    target.set("version", "6.1.2")

    sources = etree.SubElement(target, 'sources')
    for s in _source_roots:
        etree.SubElement(sources, 'source').text = s

    packages = etree.SubElement(target, 'packages')

    for group in source.findall('group'):
        for package in group.findall('package'):
            packages.append(convert_package(package))

    for package in source.findall('package'):
        _package, _line_count, _branch_count = convert_package(package)
        line_count += _line_count
        branch_count += _branch_count
        packages.append(_package)

    _line_count, _branch_count = add_counters(source, target)
    line_count += _line_count
    branch_count += _branch_count

    target.set("lines-valid", str(line_count))
    target.set("lines-covered", str(line_count))
    target.set("branches-covered", str(branch_count))
    target.set("branches-valid", str(branch_count))


def jacoco2cobertura(*, _path: str = None, _source_roots: str = None,
                     tree: etree.ElementTree = None, string: str = None,
                     return_tree: bool = False):
    if not _source_roots:
        _source_roots = "src/main/java"
    if tree:
        pass
    elif string:
        tree = etree.parse(BytesIO(string.encode('utf-8')))
    elif _path:
        tree = etree.parse(string)
    root = tree.getroot()

    into = etree.Element('coverage')
    convert_root(root, into, _source_roots)
    if return_tree:
        return etree.ElementTree(into)
    else:
        return etree.tostring(etree.ElementTree(into)).decode('utf-8')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: cover2cover.py FILENAME [SOURCE_ROOTS]")
        sys.exit(1)

    filename = sys.argv[1]
    source_roots = sys.argv[2:] if 2 < len(sys.argv) else '.'

    print(jacoco2cobertura(_path=filename, _source_roots=source_roots))
